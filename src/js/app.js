// import debounce                 from './modules/debounce';

import jQuery                   from './vendors/jquery.min';
window.$ = window.jQuery = jQuery;

import background               from './modules/background';
import browserCheck             from './modules/browser-check';
import acceptCookies            from './modules/cookies';
import inView                   from './modules/viewport';
import mainMenu                 from './modules/menu';
import featuredPostsCarousel    from './modules/featured-posts';
import districtsCarousel        from './modules/districts-carousel';
import accessible               from './modules/accesibility';

class App {
    constructor () {
        background();
        browserCheck();
        acceptCookies();
        inView();
        mainMenu();
        districtsCarousel();
        featuredPostsCarousel();
        accessible();
    }
}

document.addEventListener('DOMContentLoaded', () => {
    new App(); // eslint-disable-line no-new
});
