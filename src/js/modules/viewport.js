import scrollMonitor  from '../vendors/scrollMonitor.min';

export default function inView () {
    const el = document.querySelectorAll('[data-view]');

    function addClass () {
        if (!this.isInViewport) {
            this.watchItem.classList.remove('in-view');
        } else if (this.isInViewport) {
            this.watchItem.classList.add('in-view', 'seen');
        }
    }

    function makeWatcher (element) {
        const watcher = scrollMonitor.create(element, {top: -50, bottom: 0});

        watcher.stateChange(addClass);
        addClass.call(watcher);
    }

    if (typeof (el) !== 'undefined' && el !== null) {
        for (let i = 0; i < el.length; i++) {
            makeWatcher(el[i]);
        }
    }
}
