import Cookies       from '../vendors/cookie.min';

export default function accessible () {
    const root = document.getElementsByTagName( 'html' )[0]
    const contrast = document.querySelector('#contrast');
    const fontSize_0 = document.querySelector('#font-default');
    const fontSize_1 = document.querySelector('#font-large');
    const fontSize_2 = document.querySelector('#font-larger');

    function toggleHighContrast(event) {
        event.preventDefault();

        if (document.body.classList.contains('high-contrast')) {
            document.body.classList.remove('high-contrast');
            Cookies.remove('HighContrast');
        } else {
            document.body.classList.add('high-contrast');
            Cookies.set('HighContrast', true, {expires: 30});
        }
    }

    (function checkContrast () {
        if (Cookies.get('HighContrast')) {
            document.body.classList.add('high-contrast');
        }
    })();

    (function checkFontSize () {
        if (Cookies.get('Font_0')) {
            root.classList.add('font-size-0');
            root.classList.remove('font-size-1','font-size-2');
        }
        if (Cookies.get('Font_1')) {
            root.classList.add('font-size-1');
            root.classList.remove('font-size-0','font-size-2');
        }
        if (Cookies.get('Font_2')) {
            root.classList.add('font-size-2');
            root.classList.remove('font-size-1','font-size-0');
        }
    })();

    function defaultFontSize(event) {
        event.preventDefault();

        Cookies.set('Font_0', true, {expires: 30});
        Cookies.remove('Font_1');
        Cookies.remove('Font_2');

        root.classList.add('font-size-0');
        root.classList.remove('font-size-1','font-size-2');
    }

    function largeFontSize(event) {
        event.preventDefault();

        Cookies.remove('Font_0');
        Cookies.set('Font_1', true, {expires: 30});
        Cookies.remove('Font_2');

        root.classList.add('font-size-1');
        root.classList.remove('font-size-0','font-size-2');
    }

    function largerFontSize(event) {
        event.preventDefault();

        Cookies.remove('Font_0');
        Cookies.remove('Font_1');
        Cookies.set('Font_2', true, {expires: 30});

        root.classList.add('font-size-2');
        root.classList.remove('font-size-1','font-size-0');
    }


    if (typeof (contrast) !== 'undefined' && contrast !== null) {
        contrast.addEventListener('click', toggleHighContrast);
    }
    if (typeof (fontSize_0) !== 'undefined' && fontSize_0 !== null) {
        fontSize_0.addEventListener('click', defaultFontSize);
        fontSize_1.addEventListener('click', largeFontSize);
        fontSize_2.addEventListener('click', largerFontSize);
    }
}
