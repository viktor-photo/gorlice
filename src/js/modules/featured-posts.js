import Swiper   from '../vendors/swiper.min';
import debounce from './debounce';

export default function featuredPostsCarousel () {
    const $t = 700;
    const id = document.querySelector('#fp-carousel');
    const paging = document.querySelector('#fp-pagination');

    function setPaginationPosition () {
        const fI = $(id).find('.featured-image');
        $(paging).css({'top': fI.height() - 20 + 'px'});
    }

    if (typeof (id) !== 'undefined' && id !== null) {
        const mySwiper = new Swiper(id, {
            loop: true,
            speed: $t,
            spaceBetween: 0,
            autoplay: {
                delay: 8000,
                disableOnInteraction: false,
            },
            watchSlidesProgress: true,
            observer: true,
            effect: 'fade',
            fadeEffect: {
                crossFade: true
            },
            pagination: {
                el: paging,
                clickable: true
            }
        });

        setPaginationPosition();

        window.addEventListener('resize', debounce(function() {
            setPaginationPosition();
        }, 0));
    }
}
