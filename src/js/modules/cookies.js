import Cookies       from '../vendors/cookie.min';

export default function acceptCookies () {
    const base = document.querySelector('.cookies-notification');
    const button = document.querySelector('#close-cookies');

    function openNotification () {
        base.classList.add('active');
    }

    function closeNotification () {
        base.classList.remove('active');
    }

    if (!Cookies.get('Cookies_Accepted', true)) {
        setTimeout(()=>{
            openNotification();
        }, 2000);
    }

    if (typeof (base) !== 'undefined' && base !== null) {
        button.addEventListener('click', (e) => {
            e.preventDefault();
            Cookies.set('Cookies_Accepted', true, {expires: 30});
            closeNotification();
        });
    }
}
