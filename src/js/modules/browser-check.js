export default function browserCheck () {
    const isIE = /*@cc_on!@*/ !!document.documentMode;  // eslint-disable-line no-inline-comments
    const isEdge = /Edge\/\d./i.test(navigator.userAgent);
    const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    const isIOS = /iPhone|iPad|iPod/i.test(navigator.userAgent);
    const body = document.body;

    if (isMobile) {
        body.classList.add('mobile');
    }

    if (isIOS) {
        body.classList.add('ios');
    }

    if (isIE || isEdge) {
        body.classList.add('ie');
    }
}
