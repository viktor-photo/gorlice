<!DOCTYPE html>
<html class="no-js" lang="pl">
<head>
    <title><%= fontName %></title>
    <link href="assets/css/application.css" rel="stylesheet">
    <style>
        body { font-family: Gill Sans; text-align: center; background: #f7f7f7 }
        body > h1 { color: #666; margin: 1em 0 }
        .glyph { padding: 0 }
        .glyph > li { vertical-align: top; display: inline-block; margin: .3em .2em; width: 5em; height: 6.5em; background: #fff; border-radius: .5em; position: relative }
        .glyph > li .s { display: block; margin-top: .1em; line-height: 0 }
        .glyph-name { font-size: .8em; color: #666; display: block }
        .glyph-codepoint { color: #999; font-family: monospace }
        .icon {display:block !important}
        .icon.icon-4x:before {font-size: 16px}
    </style>
</head>
<body>
<h1><%= fontName %></h1>
<ul class="glyph"><% _.each(glyphs, function(glyph) { %>
    <li>
        <span class="<%= className %> <%=className %>-<%= glyph.name %> <%= className %>-4x"></span>
        <span class="glyph-name"><%= glyph.name %></span>
        <span class="glyph-codepoint"><%= glyph.codepoint.toString(16).toUpperCase() %></span>
    </li><% }); %>
</ul>
</body>
</html>
