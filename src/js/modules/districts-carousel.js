import Swiper   from '../vendors/swiper.min';

export default function districtsCarousel () {
    const $t = 600;
    const id = document.querySelector('#districts');

    if (typeof (id) !== 'undefined' && id !== null) {
        const mySwiper = new Swiper(id, {
            loop: true,
            speed: $t,
            slidesPerView: 5,
            centeredSlides: true,
            spaceBetween: 30,
            autoplay: {
                delay: 5000,
                disableOnInteraction: false
            },
            watchSlidesProgress: true,
            observer: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                900: {
                    slidesPerView: 3,
                    spaceBetween: 30,
                },
                600: {
                    slidesPerView: 2,
                    spaceBetween: 10,
                    centeredSlides: false
                },
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10,
                    centeredSlides: true
                }
            }
        });
    }
}
