export default function mainMenu () {
    const hamburger = document.querySelector('.hamburger-menu');
    const body = document.body;

    function animateIn () {
        body.classList.add('menu-open');
        hamburger.classList.add('active');
    }

    function animateOut () {
        body.classList.remove('menu-open');
        hamburger.classList.remove('active');
    }

    function checkState (e) {
        e.preventDefault();

        body.classList.contains('menu-open') ? animateOut() : animateIn();
    }

    if (typeof (hamburger) !== 'undefined' && hamburger !== null) {
        hamburger.addEventListener('click', checkState, false);
    }
}
