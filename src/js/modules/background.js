export default function background () {
    const bi  = document.querySelectorAll('[data-bg-image]');
    const bc  = document.querySelectorAll('[data-bg]');

    if (typeof (bi) !== 'undefined' && bi !== null) {
        for (let i = 0; i < bi.length; i++) {
            bi[i].style.backgroundImage = 'url(' + bi[i].getAttribute('data-bg-image') + ')';
        }
    }

    if (typeof (bc) !== 'undefined' && bc !== null) {
        for (let i = 0; i < bc.length; i++) {
            bc[i].style.backgroundColor = bc[i].getAttribute('data-bg');
        }
    }
}
