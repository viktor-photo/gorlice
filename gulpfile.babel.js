/*eslint-disable*/
const gulp         = require('gulp');
const lodash       = require('lodash');
const babel        = require('gulp-babel');
const sass         = require('gulp-sass');
const sourcemaps   = require('gulp-sourcemaps');
const consolidate  = require('gulp-consolidate');
const rename       = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');
const browserSync  = require('browser-sync');
const browserify   = require('browserify');
const babelify     = require('babelify');
const cssnano      = require('gulp-cssnano');
const concat       = require('gulp-concat');
const fileinclude  = require('gulp-file-include');
const source       = require('vinyl-source-stream');
const buff         = require('vinyl-buffer');
const plumber      = require('gulp-plumber');
const uglify       = require('gulp-uglify');
const purgecss     = require('gulp-purgecss');
const iconfont     = require('gulp-iconfont');
const prettyHtml   = require('gulp-pretty-html');
/*eslint-enable*/

const vendorsArray = [
    'src/js/vendors/jquery.min.js',
    'src/js/vendors/classie.min.js',
    'src/js/vendors/cookie.min.js',
    'src/js/vendors/matchHeight.min.js',
    'src/js/vendors/scrollMonitor.min.js',
    'src/js/vendors/swiper.min.js'
];

gulp.task('browser-sync', gulp.series((done) => {
    browserSync.init({
        server: {
            baseDir: 'public',
            index: './toc.html'
        },
        open: false,
        notify: false,
        logLevel: 'debug'
    });

    gulp.watch('src/sass/**/*.scss', gulp.series('sass'));
    gulp.watch('src/js/**/*.*', gulp.series('js'));
    gulp.watch('src/img/**/*.*', gulp.series('img'));
    gulp.watch(['./src/html/**/*.html', 'toc.html'], gulp.series('html'))
        .on('change', browserSync.reload);

    done();
}));

gulp.task('html', (done) => {
    gulp.src('./toc.html', {
    }).pipe(gulp.dest('./public/'));

    gulp.src(['src/html/*.html'], {
        base: 'src/html/'
    })
        .pipe(fileinclude({
            prefix: '@@',
            basepath: './src/html/partials/'
        }))
        .pipe(plumber())
        .pipe(prettyHtml({
            preserve_newlines: false,
            end_with_newline: false
        }))
        .pipe(gulp.dest('./public/'));

    done();
});

gulp.task('js', () => {
    const dst = 'public/js/';

    return browserify({
        entries: 'src/js/app.js',
        debug: true
    }).transform(babelify, {presets: ['@babel/preset-env']})
        .bundle()
        .pipe(plumber())
        .pipe(source('app.js'))
        .pipe(buff())
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(dst))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('vendorsJS', () => {
    return gulp.src(vendorsArray)
        .pipe(concat('vendors.min.js'))
        .pipe(gulp.dest('public/js/vendors/'));
});

gulp.task('fonts', () => {
    return gulp.src(['src/fonts/**/*.+(woff|woff2|eot|ttf|svg)'], {
        base: './src/'
    })
        .pipe(gulp.dest('./public'));
});

gulp.task('img', () => {
    return gulp.src(['src/img/**/*.+(png|jpg|gif|svg)'], {
        base: './src/'
    })
        .pipe(gulp.dest('./public'));
});

gulp.task('favicon', () => {
    return gulp.src(['src/favicon/**/*'], {
        base: './src/'
    })
        .pipe(gulp.dest('./public'));
});

gulp.task('iconfont', () => {
    return gulp.src(['./src/icons/*.svg'])
        .pipe(iconfont({
            fontName: 'iconfont',
            prependUnicode: false,
            formats: ['svg', 'ttf', 'eot', 'woff'],
            normalize: true,
            fontHeight: 1001
        }))
        .on('glyphs', function (glyphs) {
            const options = {
                glyphs: glyphs.map(function (glyph) {
                    return {
                        name: glyph.name,
                        codepoint: glyph.unicode[0].charCodeAt(0).toString(16).toUpperCase()
                    };
                }),
                fontName: 'iconfont',
                fontPath: '../fonts/icons/',
                className: 'icon'
            };

            gulp.src('src/icons/templates/font.template.tpl')
                .pipe(consolidate('lodash', options))
                .pipe(rename('_icons.scss'))
                .pipe(gulp.dest('src/sass/common/'));
        })
        .pipe(gulp.dest('public/fonts/icons/'));
});

gulp.task('sass', () => {
    return gulp.src('./src/sass/style.scss')
        .pipe(sourcemaps.init())
        .pipe(plumber())
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(cssnano())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./public/css'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('index:production', () => {
    gulp.src('toc.html')
        .pipe(fileinclude({
            prefix: '@@',
            basepath: './src/html/partials/'
        }))
        .pipe(plumber())
        .pipe(rename('index.html'))
        .pipe(gulp.dest('./public/'));
});

gulp.task('build:base', gulp.series('html', 'sass', 'js', 'fonts', 'iconfont', 'img', 'favicon', 'vendorsJS'));
gulp.task('build', gulp.series('build:base', 'browser-sync'));
gulp.task('default', gulp.series('build'));
